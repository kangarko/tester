Tester is a plugin intended for local test servers for debugging purposes. It allows you to enable/disable operator status, change gamemode, clear your inventory or get any item easily and without permission's check.

It contains a force-op, so use on production servers is prohibited.

For command list, type /tester in the game.
