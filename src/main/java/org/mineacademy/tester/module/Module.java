package org.mineacademy.tester.module;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.mineacademy.tester.Tester;

public abstract class Module implements Listener {

	public Module() {
		Bukkit.getPluginManager().registerEvents(this, Tester.getInstance());
	}

	public abstract void onServerStart();

	public void onServerStop() {
	}

	protected final void tellRaw(Player player, String... msgs) {
		for (final String msg : msgs)
			player.sendMessage(colorize("&7" + msg));
	}

	protected final void tell(Player player, String msg) {
		player.sendMessage(colorize("&fTester &8// &7" + msg));
	}

	protected final String colorize(String msg) {
		return ChatColor.translateAlternateColorCodes('&', msg);
	}

	public boolean isEnabled() {
		return true;
	}
}
