package org.mineacademy.tester.module;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.bukkit.Bukkit;
import org.mineacademy.tester.Tester;

public final class ModuleManager {

	private final List<Module> modules = new ArrayList<>();

	public void load() {
		try {
			for (final Class<?> cl : getClasses("org.mineacademy.tester.module.impl")) {
				final Module module = (Module) cl.newInstance();

				if (module.isEnabled())
					modules.add(module);
			}

		} catch (final Throwable e) {
			e.printStackTrace();
		}
	}

	private Set<Class<?>> getClasses(String packageName) throws Throwable {
		final Set<Class<?>> classes = new HashSet<>();

		try (final JarFile file = new JarFile(Tester.getInstance().getFile())) {
			for (final Enumeration<JarEntry> entry = file.entries(); entry.hasMoreElements();) {
				final JarEntry jarEntry = entry.nextElement();
				final String name = jarEntry.getName().replace("/", ".");

				if (name.startsWith(packageName) && name.endsWith(".class") && !name.contains("$"))
					try {
						classes.add(Class.forName(name.substring(0, name.length() - 6)));
					} catch (final NoClassDefFoundError ex) {
						Bukkit.getLogger().severe("Failed to register " + name.replace(packageName + ".", "") + ", missing library: " + ex.getMessage().replace("/", "."));
					}
			}
		}

		return classes;
	}

	public void startModules() {
		for (final Module m : modules)
			m.onServerStart();
	}

	public void stopModules() {
		for (final Module m : modules)
			m.onServerStop();
	}
}
