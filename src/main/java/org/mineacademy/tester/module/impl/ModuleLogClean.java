package org.mineacademy.tester.module.impl;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.mineacademy.tester.Tester;
import org.mineacademy.tester.module.Module;

public class ModuleLogClean extends Module {

	@Override
	public void onServerStart() {
		deleteObsoleteFiles();
		clearLogsFolder();
	}

	@Override
	public boolean isEnabled() {
		return Tester.CLEAR_LOGS;
	}

	private final void deleteObsoleteFiles() {
		final File logs = new File(getMainServerFolder(), "world");
		final List<String> toDelete = Arrays.asList("level.dat_old");

		if (logs.exists())
			for (final File f : logs.listFiles())
				if (toDelete.contains(f.getName()))
					f.delete();

	}

	private final void clearLogsFolder() {
		final File logs = new File(getMainServerFolder(), "logs");

		if (logs.exists())
			for (final File f : logs.listFiles()) {
				if (f.getName().contains("promo"))
					continue;

				f.delete();
			}
	}

	private final File getMainServerFolder() {
		return Tester.getInstance().getDataFolder().getParentFile().getParentFile();
	}
}
