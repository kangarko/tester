package org.mineacademy.tester.module.impl;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Hanging;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.tester.Tester;
import org.mineacademy.tester.module.Module;

public class ModuleCommands extends Module {

	@Override
	public void onServerStart() {
	}

	@Override
	public boolean isEnabled() {
		return Tester.ENABLE_COMMANDS;
	}

	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent event) {
		final String message = event.getMessage().toLowerCase();
		final Player player = event.getPlayer();
		final String[] args = message.split(" ");

		if ("/tester".equals(message)) {
			tellRaw(player,
					"&6Available tester commands: ",
					"- &f/o &7Enable operator status.",
					"- &f/d &7Disable operator status.",
					"- &f/gmc &7Set your gamemode to creative.",
					"- &f/gms &7Set your gamemode to survival.",
					"- &f/fly &7Toggle flight mode.",
					"- &f/speed &7Toggle slow-fast flight speed.",
					"- &f/ci &7Clears your inventory.",
					"- &f/i <item> &7Gives you items.",
					"- &f/spawn &7Teleport to server spawn.",
					"- &f/butcher &7Kill mobs, animals, arrows and items.");
			event.setCancelled(true);

		} else if ("/o".equals(message) || "/d".equals(message))
			setOperatorStatus(event, "/o".equals(message));

		else if ("/gms".equals(message)) {
			tell(player, "&6Gamemode set to survival.");

			player.setGameMode(GameMode.SURVIVAL);
			event.setCancelled(true);
		}

		else if ("/gmc".equals(message)) {
			tell(player, "&6Gamemode set to creative.");

			player.setGameMode(GameMode.CREATIVE);
			event.setCancelled(true);
		}

		else if ("/speed".equals(message)) {
			final float current = player.getFlySpeed();
			tell(player, current == 1F ? "&6Toggled speed back to normal." : "&6Toggled speed to 10X.");

			player.setFlySpeed(current == 1F ? 0.1F : 1F);
			event.setCancelled(true);
		}

		else if ("/fly".equals(message)) {
			tell(player, player.isFlying() ? "&cFlight mode has been deactivated." : "&aFlight enabled, double-press space bar to toggle.");

			player.setAllowFlight(!player.getAllowFlight());
			event.setCancelled(true);
		}

		else if ("/ci".equals(message)) {
			tell(player, "&6Your inventory has been cleared.");

			player.getInventory().clear();
			event.setCancelled(true);
		}

		else if ("/spawn".equals(message)) {
			tell(player, "&6Teleporting to server spawn.");

			player.teleport(player.getWorld().getSpawnLocation());
			event.setCancelled(true);
		}

		else if ("/setspawn".equals(message)) {
			tell(player, "&6Set server spawn to current block location.");

			final Location old = player.getLocation();
			final Location loc = new Location(old.getWorld(), old.getBlockX(), old.getY(), old.getBlockZ());

			player.getWorld().setSpawnLocation(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
			event.setCancelled(true);
		}

		else if ("/butcher".equals(message)) {
			int killed = 0;

			for (final World world : Bukkit.getWorlds())
				for (final Entity entity : world.getEntities())
					if (!(entity instanceof Hanging) && !(entity instanceof Player)) {
						Bukkit.getLogger().info("Removing entity " + entity + " from " + entity.getLocation());

						entity.remove();
						killed++;
					}

			tell(player, "&6Removed " + killed + " entities. See console for details.");
			event.setCancelled(true);
		}

		else if ("/cl".equals(message)) {

			for (final Player online : Bukkit.getOnlinePlayers())
				for (int i = 0; i < 500; i++)
					online.sendMessage(ChatColor.RESET.toString());

			event.setCancelled(true);
		}

		else if ("/i".equals(message)) {
			tell(player, "&6Usage: /i <item> or /i <item> <amount>");

			event.setCancelled(true);

		}

		else if (message.startsWith("/i ")) {
			if (args.length == 2 || args.length == 3) {
				final int amount = args.length == 3 ? Integer.parseInt(args[2]) : 64;
				final Material item = Material.getMaterial(args[1].toUpperCase());

				if (item != null) {
					tell(player, "&6You were given " + amount + "x of " + item + ".");

					player.getInventory().addItem(new ItemStack(item, amount));
				} else
					tell(player, "&cItem named " + args[1] + " does not exist on this server version.");

			} else
				tell(player, "&6Usage: /i <item> or /i <item> <amount>");

			event.setCancelled(true);
		}
	}

	private void setOperatorStatus(PlayerCommandPreprocessEvent event, boolean operator) {
		tell(event.getPlayer(), operator ? "&aYou are now an operator." : "&cYou are no longer an operator.");

		event.getPlayer().setOp(operator);
		event.setCancelled(true);
	}
}
