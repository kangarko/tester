package org.mineacademy.tester.module.impl;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.mineacademy.tester.Tester;
import org.mineacademy.tester.module.Module;

public class ModuleNoSlime extends Module {

	@Override
	public void onServerStart() {
	}

	@Override
	public boolean isEnabled() {
		return Tester.DISABLE_SLIMES;
	}

	@EventHandler
	public void onSlimeSpawn(CreatureSpawnEvent e) {
		if (Tester.DISABLE_SLIMES && e.getEntityType() == EntityType.SLIME)
			e.setCancelled(true);
	}
}
