package org.mineacademy.tester.module.impl;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;
import org.mineacademy.tester.Tester;
import org.mineacademy.tester.module.Module;

public class ModuleScheduledCommands extends Module {

	@Override
	public void onServerStart() {
		new BukkitRunnable() {

			@Override
			public void run() {
				for (String command : Tester.STARTUP_COMMANDS) {
					if (command.startsWith("/"))
						command = command.substring(1);

					System.out.println("Running command /" + command);
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
				}
			}
		}.runTask(Tester.getInstance());

		new BukkitRunnable() {

			@Override
			public void run() {
				for (String command : Tester.THRESHOLD_COMMANDS) {
					if (command.startsWith("/"))
						command = command.substring(1);

					System.out.println("Running delayed command /" + command);
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
				}
			}
		}.runTaskLater(Tester.getInstance(), 2 * 60 * 20);

	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}
