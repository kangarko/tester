package org.mineacademy.tester.module.impl;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.mineacademy.tester.Tester;
import org.mineacademy.tester.module.Module;

public class ModuleWeather extends Module {

	@Override
	public boolean isEnabled() {
		return Tester.DISABLE_WEATHER;
	}

	@Override
	public void onServerStart() {
		Bukkit.getScheduler().scheduleSyncRepeatingTask(Tester.getInstance(), () -> {
			for (final World world : Bukkit.getWorlds())
				world.setWeatherDuration(0);
		}, 20L, 20 * 5);
	}
}
