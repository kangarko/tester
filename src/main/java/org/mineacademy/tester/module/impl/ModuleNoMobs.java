package org.mineacademy.tester.module.impl;

import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.mineacademy.tester.Tester;
import org.mineacademy.tester.module.Module;

public class ModuleNoMobs extends Module {

	@Override
	public void onServerStart() {
	}

	@Override
	public boolean isEnabled() {
		return Tester.DISABLE_NATURAL_MOBS;
	}

	@EventHandler
	public void onSlimeSpawn(CreatureSpawnEvent e) {
		if (Tester.DISABLE_NATURAL_MOBS && e.getSpawnReason() == SpawnReason.NATURAL)
			e.setCancelled(true);
	}
}
