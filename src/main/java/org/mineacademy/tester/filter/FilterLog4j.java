package org.mineacademy.tester.filter;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.LifeCycle;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.message.Message;
import org.mineacademy.tester.Tester;

public class FilterLog4j implements Filter {

	/**
	 *
	 */
	public FilterLog4j() {
	}

	private Filter.Result filter(String message) {
		return allow(message) ? null : Filter.Result.DENY;
	}

	private boolean allow(String message) {
		message = Tester.stripColors(message).toLowerCase();

		for (final String filter : Tester.LOG_MESSAGES_TO_HIDE)
			if (message.contains(filter.toLowerCase()))
				return false;

		return true;
	}

	@Override
	public LifeCycle.State getState() {
		return null;
	}

	@Override
	public void initialize() {
	}

	@Override
	public void start() {
	}

	@Override
	public void stop() {
	}

	@Override
	public boolean isStarted() {
		return true;
	}

	@Override
	public boolean isStopped() {
		return false;
	}

	@Override
	public Filter.Result getOnMismatch() {
		return null;
	}

	@Override
	public Filter.Result getOnMatch() {
		return null;
	}

	@Override
	public Filter.Result filter(LogEvent event) {

		// Prevent PaperMC spam
		if (event.getLoggerName().contains("io.papermc.paper.util.PaperJvmChecker"))
			return Filter.Result.DENY;

		return filter(event.getMessage().getFormattedMessage());
	}

	@Override
	public Filter.Result filter(Logger logger, Level level, Marker marker, String message, Object... params) {
		return filter(message);
	}

	@Override
	public Filter.Result filter(Logger logger, Level level, Marker marker, String message, Object p0) {
		return filter(message);
	}

	@Override
	public Filter.Result filter(Logger logger, Level level, Marker marker, String message, Object p0, Object p1) {
		return filter(message);
	}

	@Override
	public Filter.Result filter(Logger logger, Level level, Marker marker, String message, Object p0, Object p1, Object p2) {
		return filter(message);
	}

	@Override
	public Filter.Result filter(Logger logger, Level level, Marker marker, String message, Object p0, Object p1, Object p2, Object p3) {
		return filter(message);
	}

	@Override
	public Filter.Result filter(Logger logger, Level level, Marker marker, String message, Object p0, Object p1, Object p2, Object p3, Object p4) {
		return filter(message);
	}

	@Override
	public Filter.Result filter(Logger logger, Level level, Marker marker, String message, Object p0, Object p1, Object p2, Object p3, Object p4, Object p5) {
		return filter(message);
	}

	@Override
	public Filter.Result filter(Logger logger, Level level, Marker marker, String message, Object p0, Object p1, Object p2, Object p3, Object p4, Object p5, Object p6) {
		return filter(message);
	}

	@Override
	public Filter.Result filter(Logger logger, Level level, Marker marker, String message, Object p0, Object p1, Object p2, Object p3, Object p4, Object p5, Object p6, Object p7) {
		return filter(message);
	}

	@Override
	public Filter.Result filter(Logger logger, Level level, Marker marker, String message, Object p0, Object p1, Object p2, Object p3, Object p4, Object p5, Object p6, Object p7, Object p8) {
		return filter(message);
	}

	@Override
	public Filter.Result filter(Logger logger, Level level, Marker marker, String message, Object p0, Object p1, Object p2, Object p3, Object p4, Object p5, Object p6, Object p7, Object p8, Object p9) {
		return filter(message);
	}

	@Override
	public Filter.Result filter(Logger logger, Level level, Marker marker, Object message, Throwable t) {
		return filter(message.toString());
	}

	@Override
	public Filter.Result filter(Logger logger, Level level, Marker marker, Message message, Throwable t) {
		return filter(message.getFormattedMessage());
	}

	public static void applyFilter() {
		((Logger) LogManager.getRootLogger()).addFilter(new FilterLog4j());
	}
}
