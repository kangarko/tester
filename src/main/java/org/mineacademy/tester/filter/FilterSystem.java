package org.mineacademy.tester.filter;

import java.io.OutputStream;
import java.io.PrintStream;

import org.mineacademy.tester.Tester;

import lombok.SneakyThrows;

public class FilterSystem extends PrintStream {
	public FilterSystem(OutputStream out) {
		super(out, true);
	}

	@Override
	public void println(String s) {
		if (shouldFilter(s))
			return;

		super.println(s);
	}

	@SneakyThrows
	private boolean shouldFilter(String message) {
		message = message.toLowerCase();

		for (final String filter : Tester.LOG_MESSAGES_TO_HIDE)
			if (Tester.stripColors(message).contains(filter.toLowerCase()))
				return true;

		return false;
	}

	public static void applyFilter() {
		System.setOut(new FilterSystem(System.out));
	}
}
