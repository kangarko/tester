package org.mineacademy.tester.filter;

import java.util.logging.Filter;
import java.util.logging.LogRecord;

import org.mineacademy.tester.Tester;

public class FilterLegacy implements Filter {

	@Override
	public boolean isLoggable(LogRecord record) {
		String message = record.getMessage();

		if (message == null || message.isEmpty())
			return false;

		message = Tester.stripColors(message).toLowerCase();

		for (final String filter : Tester.LOG_MESSAGES_TO_HIDE)
			if (message.contains(filter.toLowerCase()))
				return false;

		return true;
	}
}