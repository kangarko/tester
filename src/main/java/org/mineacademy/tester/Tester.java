package org.mineacademy.tester;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.List;
import java.util.logging.Filter;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.mineacademy.tester.filter.FilterLegacy;
import org.mineacademy.tester.filter.FilterLog4j;
import org.mineacademy.tester.filter.FilterSystem;
import org.mineacademy.tester.module.ModuleManager;

import lombok.Getter;
import lombok.SneakyThrows;

public class Tester extends JavaPlugin implements Listener {

	@Getter
	private static Tester instance;

	// -----------------------------------------------------------------
	// Static settings
	// -----------------------------------------------------------------

	public static final String FOLDER_NAME = "Archive of Tester";

	public static Boolean ENABLE_COMMANDS, DISABLE_WEATHER, CLEAR_LOGS, DISABLE_NATURAL_MOBS, DISABLE_SLIMES;
	public static List<String> LOG_MESSAGES_TO_HIDE, PLUGINS_TO_NOT_MOVE, STARTUP_COMMANDS, THRESHOLD_COMMANDS;

	static {
		loadSettings();

		try {
			FilterLog4j.applyFilter();

		} catch (final Throwable ex) {

			// MC too old
			if (!ex.getMessage().contains("org/apache/logging/log4j/core/Filter"))
				ex.printStackTrace();
		}

		FilterSystem.applyFilter();

		FolderCleaner.scan();
	}

	@SneakyThrows
	private static final void loadSettings() {
		final File file = new File("plugins/" + FOLDER_NAME, "config.yml");

		if (!file.exists()) {
			final InputStream is = Tester.class.getResourceAsStream("/config.yml");

			if (!file.getParentFile().exists())
				file.getParentFile().mkdir();

			Files.copy(is, file.toPath());
		}

		final YamlConfiguration config = YamlConfiguration.loadConfiguration(file);

		CLEAR_LOGS = config.getBoolean("Clear_Logs");
		ENABLE_COMMANDS = config.getBoolean("Enable_Commands");
		DISABLE_WEATHER = config.getBoolean("Disable_Weather");
		DISABLE_NATURAL_MOBS = config.getBoolean("Disable_Natural_Mobs");
		DISABLE_SLIMES = config.getBoolean("Disable_Slimes", false);
		LOG_MESSAGES_TO_HIDE = config.getStringList("Hide_Log_Messages");
		PLUGINS_TO_NOT_MOVE = config.getStringList("Do_Not_Move_Plugins");
		STARTUP_COMMANDS = config.getStringList("Startup_Commands");
		THRESHOLD_COMMANDS = config.getStringList("Threshold_Commands");
	}

	// -----------------------------------------------------------------

	private final ModuleManager modules = new ModuleManager();

	@Override
	public final void onEnable() {
		instance = this;

		modules.load();
		modules.startModules();

		getServer().getPluginManager().registerEvents(this, this);
		enableFilter();
	}

	private void enableFilter() {
		final Filter filter = new FilterLegacy();

		for (final Plugin plugin : getServer().getPluginManager().getPlugins())
			plugin.getLogger().setFilter(filter);

		getServer().getLogger().setFilter(filter);
	}

	@Override
	public final void onDisable() {
		modules.stopModules();
	}

	@Override
	public final File getFile() {
		return super.getFile();
	}

	public static String stripColors(String message) {
		return message == null || message.isEmpty() ? "" : message.replaceAll("(" + ChatColor.COLOR_CHAR + "|&)([0-9a-fk-or])", "");
	}
}
