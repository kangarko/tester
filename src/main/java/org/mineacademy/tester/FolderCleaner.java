package org.mineacademy.tester;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;

import com.google.common.io.Files;

import lombok.NonNull;
import lombok.SneakyThrows;

public class FolderCleaner {

	public static final void scan() {
		try {
			scan0();

		} catch (final Throwable throwable) {
			throwable.printStackTrace();

			Bukkit.getLogger().severe("Error preparing up, systems shut down");
			System.exit("666".length());
		}
	}

	private static final void scan0() throws IOException {
		final Set<String> pluginNames = new HashSet<>();

		final File pluginsFolder = new File("plugins");
		final File archiveFolder = new File(pluginsFolder, Tester.FOLDER_NAME);
		final File[] pluginFiles = pluginsFolder.listFiles();

		if (!archiveFolder.exists())
			archiveFolder.mkdir();

		//
		// Load plugin names
		//
		for (final File file : pluginFiles) {

			// Only target .jar files
			if (file.isDirectory() || !file.getName().endsWith(".jar"))
				continue;

			try (JarFile jarFile = new JarFile(file)) {
				try {
					ZipEntry pluginYml = jarFile.getEntry("plugin.yml");

					if (pluginYml == null)
						pluginYml = jarFile.getEntry("paper-plugin.yml");

					if (pluginYml == null) {
						System.out.println("--------------------------------------------------------");
						System.out.println("Cannot locate plugin.yml or paper-plugin.yml in " + file);
						System.out.println("Unsupported plugin detected, shutting down.");
						System.out.println("--------------------------------------------------------");

						System.exit(0);
						return;
					}

					final InputStream is = jarFile.getInputStream(pluginYml);
					final YamlConfiguration config = loadConfig(is);
					Objects.requireNonNull(config, "Failed to open plugin.yml in " + file);

					pluginNames.add(config.getString("name"));

				} catch (final Throwable t) {
					System.out.println("--------------------------------------------------------");
					System.out.println("Error processing plugin.yml in " + file);
					System.out.println("Unexpected error detected, shutting down.");
					System.out.println("--------------------------------------------------------");

					t.printStackTrace();
					System.exit(0);
				}
			}
		}

		//
		// Check for collisions
		//
		for (final File archiveFile : archiveFolder.listFiles())
			if (archiveFile.isDirectory()) {
				final String name = archiveFile.getName();
				final File pluginFolder = new File(pluginsFolder, name);

				if (name.equals(".DS_Store") || name.equals(".paper-remapped"))
					continue;

				if (pluginFolder.exists() && !Tester.PLUGINS_TO_NOT_MOVE.contains(name))
					throw new RuntimeException("Found the same folder name in Archive/ and plugins/, remove one: " + pluginFolder.getAbsolutePath());
			}

		//
		// Move files from plugins/ to Archive/ if such plugin does not exists
		//
		for (final File directory : pluginFiles) {

			// Target directories
			if (!directory.isDirectory())
				continue;

			final String name = directory.getName();

			if (name.equals(".DS_Store") || name.equals(".paper-remapped"))
				continue;

			if (!pluginNames.contains(name) && !Tester.PLUGINS_TO_NOT_MOVE.contains(name) && !name.equals(Tester.FOLDER_NAME)) {
				System.out.println("[plugins > Archive] Moving " + name + " to Archive/");

				Files.move(directory, new File(archiveFolder, name));
			}
		}

		//
		// Move files from Archive/ to plugins/ if such plugin exists
		//

		for (final File archiveFile : archiveFolder.listFiles()) {

			// Target directories
			if (!archiveFile.isDirectory())
				continue;

			final String name = archiveFile.getName();

			if (name.equals(".DS_Store") || name.equals(".paper-remapped"))
				continue;

			if (pluginNames.contains(name) && !Tester.PLUGINS_TO_NOT_MOVE.contains(name) && !name.equals(Tester.FOLDER_NAME)) {
				System.out.println("[Archive > plugins] Moving " + name + " to plugins/");

				Files.move(archiveFile, new File(pluginsFolder, name));
			}
		}
	}

	@SneakyThrows
	private static YamlConfiguration loadConfig(@NonNull InputStream stream) {
		final YamlConfiguration config = new YamlConfiguration();
		final InputStreamReader reader = new InputStreamReader(stream, StandardCharsets.UTF_8);
		final StringBuilder builder = new StringBuilder();

		try (final BufferedReader input = new BufferedReader(reader)) {
			String line;

			while ((line = input.readLine()) != null) {
				builder.append(line);
				builder.append('\n');
			}
		}

		config.loadFromString(builder.toString());

		return config;
	}
}
